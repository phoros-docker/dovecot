FROM alpine:latest

ENV S6_OVERLAY_VERSION v1.22.1.0
ENV DOCKERIZE_VERSION v2.1.0

# Terminate all s6 childs when init stage fails
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS 2

# Create virtual mail user and group
RUN adduser -h /var/mail -D -u 5000 -g vmail vmail

# Update package list and install packages, download and compile additional Dovecot plugins,
# download s6-overlay, download dockerize
RUN apk add --update --no-cache --no-progress --virtual .build-deps autoconf automake dovecot-dev gcc git libtool make musl-dev \
    && apk add --update --no-cache --no-progress inotify-tools dovecot dovecot-dev dovecot-lmtpd dovecot-mysql dovecot-pigeonhole-plugin \
    && git clone http://git.sipsolutions.net/dovecot-antispam.git/ /tmp/dovecot-antispam/ \
    && cd /tmp/dovecot-antispam/ \
    && make \
    && make install INSTALLDIR=/usr/lib/dovecot \
    && rm -rf /tmp/dovecot-antispam/ \
    && apk del .build-deps \
    && cd / \
    && wget https://github.com/just-containers/s6-overlay/releases/download/${S6_OVERLAY_VERSION}/s6-overlay-amd64.tar.gz \
    && tar -C / -xzvf s6-overlay-amd64.tar.gz \
    && rm s6-overlay-amd64.tar.gz \
    && wget https://github.com/presslabs/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && sed -E -i /etc/dovecot/dovecot.conf -e 's/^#(listen =.*)$/\1/'

# Copy required assets for s6-overlay
COPY ./assets/s6-overlay/ /etc/

# Copy configuration
COPY ./config/ /

# Fix permissions
RUN chmod 755 /etc/ \
    && chmod -R a=r,u+w,a+X /etc/dovecot/

EXPOSE 993 4190
VOLUME ["/var/mail/"]
ENTRYPOINT ["/init"]

CREATE TABLE `aliases` (
  `source` varchar(255) NOT NULL,
  `source_domain` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `destination_domain` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`source`,`source_domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Postfix - Virtual Aliases';

CREATE TABLE `domains` (
  `domain` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  `maxquota` int(10) NOT NULL DEFAULT 0,
  `transport` varchar(255) DEFAULT NULL,
  `backupmx` tinyint(1) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Postfix - Virtual Domains';

CREATE TABLE `mailboxes` (
  `username` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `maildir` varchar(255) NOT NULL DEFAULT '',
  `quota` int(10) NOT NULL DEFAULT 0,
  `domain` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `sendonly` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`username`,`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Postfix - Virtual Mailboxes';

CREATE TABLE `tlspolicies` (
  `domain` varchar(255) NOT NULL,
  `policy` enum('none','may','encrypt','dane','dane-only','fingerprint','verify','secure') NOT NULL,
  `params` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Postfix - TLS Policies';

CREATE TABLE `vacations` (
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `cache` text NOT NULL,
  `domain` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Postfix - Virtual Vacation';
